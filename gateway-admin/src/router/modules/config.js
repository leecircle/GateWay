/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const configRouter = {
  path: '/config',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  name: 'config',
  meta: {
    title: 'config',
    icon: 'config'
  },
  children: [{
      path: '',
      component: () => import('@/views/config/index'),
      name: 'config-service',
      meta: {
        title: 'configService'
      }
    },
    {
      path: 'collection',
      component: () => import('@/views/config/collection'),
      name: 'collection',
      meta: {
        title: 'collection'
      }
    }
  ]
}
export default configRouter
