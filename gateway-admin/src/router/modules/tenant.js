/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const tenantRouter = {
  path: '/tenants',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  name: 'tenants',
  meta: {
    title: 'tenant',
    icon: 'tree',
    permissions: ['Admin.Tenant']
  },
  children: [
    {
      path: '',
      component: () => import('@/views/tenants/list'),
      name: 'tenants-list',
      meta: {
        title: 'tenant',
        permissions: ['Admin.Tenant.Manage']
      }
    }
  ]
}
export default tenantRouter
