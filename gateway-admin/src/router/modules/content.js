/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const contentRouter = {
    path: '/content',
    component: Layout,
    redirect: 'noRedirect',
    alwaysShow: true, // will always show the root menu
    name: 'content',
    meta: {
        title: 'content',
        icon: 'documentation',
        permissions: ['Admin.Content']
    },
    children: [
        {
            path: '/tags',
            component: () => import('@/views/content/tags'),
            name: 'tags-list',
            meta: {
                title: 'tag',
                permissions: ['Admin.Tag.Manage']
            }
        },
        {
            path: '/recommends',
            component: () => import('@/views/content/recommends'),
            name: 'recommends-list',
            meta: {
                title: 'recommend',
                permissions: ['Admin.Recommend.Manage']
            }
        }
    ]
}
export default contentRouter
