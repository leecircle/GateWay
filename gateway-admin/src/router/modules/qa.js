/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const qaRouter = {
    path: '/qa',
    component: Layout,
    redirect: 'noRedirect',
    alwaysShow: true, // will always show the root menu
    name: 'qa',
    meta: {
        title: 'qa',
        icon: 'message',
        permissions: ['Admin.QA']
    },
    children: [
        {
            path: 'questions',
            component: () => import('@/views/qa/questions'),
            name: 'questions-list',
            meta: {
                title: 'question',
                permissions: ['Admin.Question.Manage']
            }
        },
        {
            path: 'answers',
            component: () => import('@/views/qa/answers'),
            name: 'answers-list',
            meta: {
                title: 'answer',
                permissions: ['Admin.Answer.Manage']
            }
        }
    ]
}
export default qaRouter
