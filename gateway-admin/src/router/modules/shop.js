/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const shopRouter = {
    path: '/shops',
    component: Layout,
    redirect: 'noRedirect',
    alwaysShow: true, // will always show the root menu
    name: 'shops',
    meta: {
        title: 'shop',
        icon: 'shopping',
        permissions: ['Admin.Shop']
    },
    children: [
        {
            path: 'products',
            component: () => import('@/views/shops/products/route-view'), // 如果路由是多级目录，比如有三级路由嵌套的情况下，需要手动在二级目录的根文件下添加一个 <router-view>。
            redirect: 'noRedirect',
            alwaysShow: true, // will always show the root menu
            name: 'products',
            meta: {
                title: 'product',
                permissions: ['Admin.Shop.Product.Manage']
            },
            children: [
                {
                    path: '',
                    component: () => import('@/views/shops/products'),
                    name: 'product-list',
                    meta: {
                        title: 'product',
                        permissions: ['Admin.Shop.Product.Manage']
                    }
                },
                {
                    path: '/categories/100401',
                    component: () => import('@/views/content/categories'),
                    name: 'product-categories-list',
                    meta: {
                        title: 'category',
                        permissions: ['Admin.Category.Manage'],
                        moduleTypeId: '100401'
                    }
                },
                {
                    path: 'specifications',
                    component: () => import('@/views/shops/products/specifications'),
                    name: 'product-specifications',
                    meta: {
                        title: 'specification',
                        permissions: ['Admin.Shop.Specification.Manage']
                    }
                }
            ]
        },
        {
            path: 'express',
            component: () => import('@/views/shops/express'),
            name: 'express-list',
            meta: {
                title: 'express',
                permissions: ['Admin.Shop.Express.Manage']
            }
        },
        {
            path: 'orders',
            component: () => import('@/views/shops/orders'),
            name: 'order-list',
            meta: {
                title: 'order',
                permissions: ['Admin.Shop.Order.Manage']
            }
        },
        {
            path: 'markting',
            component: () => import('@/views/shops/marktings/route-view'), // 如果路由是多级目录，比如有三级路由嵌套的情况下，需要手动在二级目录的根文件下添加一个 <router-view>。
            redirect: 'noRedirect',
            alwaysShow: true, // will always show the root menu
            name: 'marktings',
            meta: {
                title: 'markting',
                permissions: ['Admin.Shop.Marketing']
            },
            children: [
                {
                    path: 'coupons',
                    component: () => import('@/views/shops/marktings/coupons'),
                    name: 'coupon-list',
                    meta: {
                        title: 'coupon',
                        permissions: ['Admin.Shop.Marketing.Coupon.Manage']
                    }
                },
                {
                    path: 'purchase',
                    component: () => import('@/views/shops/marktings/purchase'),
                    name: 'purchase-list',
                    meta: {
                        title: 'purchase',
                        permissions: ['Admin.Shop.Marketing.PurchaseLimitation.Manage']
                    }
                }
            ]
        }
    ]
}
export default shopRouter
