/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const settingsRouter = {
  path: '/settings',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  name: 'settings',
  meta: {
    title: 'settingsManage',
    icon: 'setting'
    // permissions: ['Admin.Setting']
  },
  children: [
  ]
}
export default settingsRouter
