/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const demoRouter = {
    path: '/demo',
    component: Layout,
    redirect: 'noRedirect',
    alwaysShow: true, // will always show the root menu
    name: 'demo',
    meta: {
        title: 'demo',
        icon: 'example',
        permissions: ['Admin']
    },
    children: [
        {
            path: 'chat',
            component: () => import('@/views/demo/chat'),
            name: 'demo-chat',
            meta: {
                title: 'chat',
                permissions: ['Admin']
            }
        },
        {
            path: 'locale',
            component: () => import('@/views/demo/locale'),
            name: 'demo-locale',
            meta: {
                title: 'locale',
                permissions: ['Admin']
            }
        }
    ]
}
export default demoRouter
