/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const orderRouter = {
  path: '/order',
  component: Layout,
  redirect: 'noRedirect',
  // alwaysShow: true, // will always show the root menu
  name: 'order',
  children: [{
      path: 'index',
      component: () => import('@/views/order/index'),
      name: 'orderList',
      meta: {
        title: 'orderList',
        icon: 'documentation'
      }
    },
    {
      path: 'info',
      component: () => import('@/views/order/info'),
      name: 'orderInfo',
      hidden: true,
      meta: {
        title: 'orderInfo'
      }
    },
    {
      path: 'create',
      component: () => import('@/views/order/create'),
      name: 'orderCreate',
      hidden: true,
      meta: {
        title: 'orderCreate'
      }
    },
    {
      path: 'detail',
      component: () => import('@/views/order/detail'),
      name: 'orderDetail',
      hidden: true,
      meta: {
        title: 'orderDetail'
      }
    },
    {
      path: 'result',
      component: () => import('@/views/order/result'),
      name: 'orderResult',
      hidden: true,
      meta: {
        title: 'orderResult'
      }
    }
  ]
}
export default orderRouter
