import Vue from 'vue'

import { getCookieValue } from '@/utils/utils'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets
import Element, { Loading } from 'element-ui'
import './styles/element-variables.scss'

import '@/styles/index.scss' // global css
import App from './App'
import store from './store'
import router from './router'

import i18n, { getLanguageItem } from './lang' // internationalization
import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log
import permission from '@/directive/permission/index.js'

import * as filters from './filters' // global filters
import abp from 'abp'
import _ from 'lodash'
import moment from 'moment'
import 'moment/locale/zh-cn';

import JSEncrypt from 'jsencrypt/bin/jsencrypt.min.js';

Vue.prototype.$getRsaCode = function(str) { // 注册方法
  const pubKey = `MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqiuhn7yNjdMTSWoM0SC9lyyHn0Wp/jAMNeZYlBMqqTFyDq807vfqljbG8I1deOhbX8w71xPU7mhg6vfjfWdva2Kq+gE0dcqDrd04DUi8HkjEbQYfLw/VM+MFOyBcucg4CvTjiVSed8sLL00j4MkcqgE1KNiZ0SPh5HEB50couIkgFpfS+EL5lRO2kht0JItotsuVzcCPY2e5ueiVQ9tBmCBEQ7CdRQt/JdzRDqjGX7CyPJAQOnVY1dmI+2eGZobDzcP2/ncky/y4KB3DG/J57USM/+Q9qXbs3jiVCoqz1Q+AittUlfIVGO82PWSJxNoM02f+L2aTIpQSnd3c3ZvAPwIDAQAB`;// ES6 模板字符串 引用 rsa 公钥
  const encryptStr = new JSEncrypt();
  encryptStr.setPublicKey(pubKey); // 设置 加密公钥
   // 进行加密
  return encryptStr.encrypt(str.toString());
}

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */
// import { mockXHR } from '../mock'
// if (process.env.NODE_ENV === 'production') {
//   mockXHR()
// }

Vue.use(Element, {
  size: getCookieValue('size') || 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value)
})

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.use(permission)
// permission.install(Vue)

// Vue.directive('permission',permission)

Vue.config.productionTip = false

const loading = Loading.service({
  lock: true,
  text: '正在加载...',
  spinner: 'el-icon-loading',
  background: 'rgba(0, 0, 0, 0.9)'
})

store.dispatch('app/getAll').then((result) => {
  _.merge(abp, result)
  abp.log.level = abp.log.levels.WARN;
  const item = getLanguageItem(store.getters.language)
  i18n.locale = item.vue;
  moment.locale(item.moment)
  loading.close()

  new Vue({
    el: '#app',
    router,
    store,
    i18n,
    render: h => h(App)
  })
}).catch((result) => {
  console.log('error', result);
});
