import request from '@/utils/request'

export function getVersion() {
  return request({
    url: '/version',
    method: 'get'
  })
}

export function setAllVersion() {
  return request({
    url: '/version/all',
    method: 'post'
  })
}

export function setDgVersion() {
  return request({
    url: '/version/dg',
    method: 'post'
  })
}
