import request from '@/utils/request'

export function getAllBnetCard() {
  return request({
    url: '/bnetcard/all',
    method: 'get'
  })
}

export function getBnetCardById(id) {
  return request({
    url: '/bnetcard?id=' + id,
    method: 'get'
  })
}

export function updateBnetCard(data) {
  return request({
    url: '/bnetcard',
    method: 'put',
    data
  })
}
