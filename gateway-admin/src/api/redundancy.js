import request from '@/utils/request'

export function getRedundancy() {
  return request({
    url: '/api/redundancy',
    method: 'get'
  })
}

export function creatRedundancy(data) {
  return request({
    url: '/api/redundancy',
    method: 'post',
    data
  })
}

export function deleteRedundancy(id) {
  return request({
    url: '/api/redundancy?id=' + id,
    method: 'delete'
  })
}
