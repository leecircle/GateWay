import request from '@/utils/request'

export function getShortDataElementByPortId(portId) {
  return request({
    url: '/dataElement/short?portId=' + portId,
    method: 'get'
  })
}
export function exportDataElement(equipmentId) {
  return request({
    url: '/export/dataElement/' + equipmentId,
    method: 'get',
    responseType: 'blob'
  })
}

export function importDataElement(physicalPortId, equipmentId, param) {
  return request({
    url: '/import/dataElement/' + physicalPortId + '/' + equipmentId,
    method: 'post',
    data: param
  })
}

export function getShortPageDataElement(query) {
  return request({
    url: '/dataElement/shortpage',
    method: 'get',
    params: query
  })
}
export function getFullDataElementByPortId(portId) {
  return request({
    url: '/dataElement/full?portId=' + portId,
    method: 'get'
  })
}

export function getFullPageDataElement(query) {
  return request({
    url: '/dataElement/fullpage',
    method: 'get',
    params: query
  })
}

export function getValueDataElementByPortId(portId) {
  return request({
    url: '/dataElement/value?portId=' + portId,
    method: 'get'
  })
}

export function getValuePageDataElement(query) {
  return request({
    url: '/dataElement/valuepage',
    method: 'get',
    params: query
  })
}

export function getDataElementById(id) {
  return request({
    url: '/dataElement?id=' + id,
    method: 'get'
  })
}

export function createDataElement(data) {
  return request({
    url: '/dataElement/create',
    method: 'post',
    data
  })
}

export function updateDataElement(data) {
  return request({
    url: '/dataElement/update',
    method: 'put',
    data
  })
}

export function deleteDataElement(ids) {
  return request({
    url: '/dataElement',
    method: 'delete',
    data: ids
  })
}
