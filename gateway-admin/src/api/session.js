import request from '@/utils/request'

export function getCurrentLoginInformations() {
  return request({
    url: '/auth/info',
    method: 'get'
  })
}
