import request from '@/utils/request'

export function getShortPhysicalPort() {
  return request({
    url: '/physicalPort/short',
    method: 'get'
  })
}
export function getFullPhysicalPort() {
  return request({
    url: '/physicalPort/full',
    method: 'get'
  })
}
export function getShortPhysicalPortWithEquipment() {
  return request({
    url: '/physicalPort/short-with-equipment',
    method: 'get'
  })
}

export function getPhysicalPortById(id) {
  return request({
    url: '/physicalPort?id=' + id,
    method: 'get'
  })
}
export function getPortIdList() {
  return request({
    url: '/physicalPort/getPortIdList',
    method: 'get'
  })
}

export function getPortTypeById(id) {
  return request({
    url: '/physicalPort/getPortTypeById?id=' + id,
    method: 'get'
  })
}

export function updatePhysicalPort(data) {
  return request({
    url: '/physicalPort/update',
    method: 'put',
    data
  })
}

export function updatePhysicalPortStatus(data) {
  return request({
    url: '/physicalPort/updateStatus',
    method: 'post',
    data
  })
}

export function createPhysicalPort(data) {
  return request({
    url: '/physicalPort/create',
    method: 'post',
    data
  })
}

export function deletePhysicalPort(ids) {
  return request({
    url: '/physicalPort',
    method: 'delete',
    data: ids
  })
}

export function exportPhysicalPort() {
  return request({
    url: '/export/physicalPort',
    method: 'get',
    responseType: 'blob'
  })
}

export function importPhysicalPort(param) {
  return request({
    url: '/import/physicalPort',
    method: 'post',
    data: param
  })
}
