import request from '@/utils/request'

export function getRolePagingList(query) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/roleInfos',
    method: 'get',
    params: query
  })
}
export function getRoleForEdit(id) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/roleInfos/' + id,
    method: 'get'
  })
}
export function createOrUpdateRole(data) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/roleInfos',
    method: 'post',
    data
  })
}
export function deleteRole(id) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/roleInfos/' + id,
    method: 'delete'
  })
}

export function updateRolePermissions(id, data) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/roleInfos/' + id + '/authorize',
    method: 'put',
    data
  })
}
