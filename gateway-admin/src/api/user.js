import request from '@/utils/request'
import qs from 'qs'

export function getUserPagingList() {
  return request({
    url: '/api/users',
    method: 'get'
  })
}

export function resetPassword(id) {
  return request({
    url: '/api/users/resetPass/' + id,
    method: 'post'
  })
}

export function getUserAll(query) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/allUserInfos',
    method: 'get',
    params: query
  })
}

export function getUserForEdit(id) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/userInfos/' + id,
    method: 'get'
  })
}

export function updateUserInfosMonthlyPayAccount(id, data) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/userInfosMonthlyPayAccount/' + id,
    method: 'put',
    data
  })
}
export function createOrUpdateUser(data) {
  return request({
    url: '/api/users',
    method: 'post',
    data
  })
}
export function deleteUsers(ids) {
  return request({
    url: '/api/users',
    method: 'delete',
    data: ids
  })
}
export function changePassword(data) {
  return request({
    url: '/api/services/app/UserAdmin/ChangePasswordAsync',
    method: 'post',
    data
  })
}
export function getRolesName(id) {
  return request({
    url: '/api/services/app/UserAdmin/GetRolesNameAsync',
    method: 'get',
    params: {
      id
    }
  })
}

export function setRoles(data) {
  return request({
    url: '/api/services/app/UserAdmin/SetRolesAsync',
    method: 'post',
    data
  })
}
export function getUserPermissions(id) {
  return request({
    url: '/api/services/app/UserAdmin/GetUserPermissionsAsync',
    method: 'get',
    params: {
      id
    }
  })
}
export function updateUserPermissions(data) {
  return request({
    url: '/api/services/app/UserAdmin/UpdateUserPermissionsAsync',
    method: 'put',
    data
  })
}
export function resetAllPermissions(data) {
  return request({
    url: '/api/services/app/UserAdmin/ResetAllPermissionsAsync',
    method: 'post',
    data
  })
}

export function getPasswordComplexitySetting() {
  return request({
    url: '/api/services/app/User/GetPasswordComplexitySetting',
    method: 'get'
  })
}
