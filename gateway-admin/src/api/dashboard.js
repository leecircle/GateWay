import request from '@/utils/request'

export function getAdminDashboard() {
  return request({
    url: '/ngexpress/api/ngexpress/v1/AdminDashboard',
    method: 'get'
  })
}
