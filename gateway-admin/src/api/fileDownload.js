import request from '@/utils/request'

export function getFileList() {
  return request({
    url: '/file',
    method: 'get'
  })
}

export function downloadFile(name) {
  return request({
    url: '/file/download/' + name,
    method: 'get',
    responseType: 'blob'
  })
}
