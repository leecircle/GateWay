import request from '@/utils/request'

export function getAllEquipment() {
  return request({
    url: '/equipment/all',
    method: 'get'
  })
}
export function getAllEquipmentShort() {
  return request({
    url: '/equipment/all-short',
    method: 'get'
  })
}
export function getEquipmentByPortId(portId) {
  return request({
    url: '/equipment/all-by-portId?portId=' + portId,
    method: 'get'
  })
}
export function getEquipmentById(id) {
  return request({
    url: '/equipment?id=' + id,
    method: 'get'
  })
}

export function updateEquipment(data) {
  return request({
    url: '/equipment/update',
    method: 'put',
    data
  })
}

export function createEquipment(data) {
  return request({
    url: '/equipment/create',
    method: 'post',
    data
  })
}

export function deleteEquipment(ids) {
  return request({
    url: '/equipment',
    method: 'delete',
    data: ids
  })
}
