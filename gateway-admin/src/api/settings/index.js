import request from '@/utils/request'

export function changeLanguage(data) {
  return request({
    url: '/api/services/app/Settings/ChangeLanguageAsync',
    method: 'post',
    data: data
  })
}

export function getCachePagingList(query) {
  return request({
    url: '/api/services/app/Settings/GetCachePagingList',
    method: 'get',
    params: query
  })
}

export function clearCache(name) {
  return request({
    url: '/api/services/app/Settings/ClearCache',
    method: 'post',
    params: { name }
  })
}

export function getDnsConfig() {
  return request({
    url: '/sys/dns',
    method: 'get'
  })
}

export function setDnsConfig(dns) {
  return request({
    url: '/sys/dns',
    method: 'post',
    data: { dns: dns }
  })
}

export function setDatetime(time) {
  return request({
    url: '/sys/datetime',
    method: 'post',
    data: time
  })
}

export function dbInit() {
  return request({
    url: '/sys/dbinit',
    method: 'post'
  })
}

export function reBoot() {
  return request({
    url: '/sys/reboot',
    method: 'post'
  })
}

export function getPing(query) {
  return request({
    url: '/sys/ping',
    method: 'get',
    params: query
  })
}
export function getTraceroute(query) {
  return request({
    url: '/sys/traceroute',
    method: 'get',
    params: query
  })
}
