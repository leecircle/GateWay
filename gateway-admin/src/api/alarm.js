import request from '@/utils/request'

export function getAlarmEquip() {
  return request({
    url: '/alarm/equip',
    method: 'get'
  })
}

export function getLivePageAlarm(query) {
  return request({
    url: '/alarm/all-live-pageable',
    method: 'get',
    params: query
  })
}
