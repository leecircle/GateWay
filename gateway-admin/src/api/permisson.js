import request from '@/utils/request'

export function getAllPermissions() {
  return request({
    url: '/ngexpress/api/ngexpress/v1/authorityInfos',
    method: 'get'
  })
}

export function getAuthorityInfoUsingGET(id) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/authorityInfos/' + id,
    method: 'get'
  })
}

export function createAuthorityInfoUsingPOST(data) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/authorityInfos',
    method: 'post',
    data
  })
}

export function updateAuthorityInfoUsingPUT(data) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/authorityInfos/' + data.id,
    method: 'put',
    data
  })
}

export function deleteAuthorityInfoUsingDELETE(id) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/authorityInfos/' + id,
    method: 'delete'
  })
}
