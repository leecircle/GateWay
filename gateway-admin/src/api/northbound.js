import request from '@/utils/request'

export function getAllNorthbound() {
  return request({
    url: '/gateway-northbound/full',
    method: 'get'
  })
}

export function getNorthboundWithEquipment() {
  return request({
    url: '/gateway-northbound/full-with-equipment',
    method: 'get'
  })
}

export function getShortNorthbound() {
  return request({
    url: '/gateway-northbound/short',
    method: 'get'
  })
}

export function getNorthboundEquipment() {
  return request({
    url: '/gateway-northbound/equip',
    method: 'get'
  })
}

export function getNorthboundDataElement(query) {
  return request({
    url: '/gateway-northbound/dataElement',
    method: 'get',
    params: query
  })
}

export function getNorthboundDataCache(query) {
  return request({
    url: '/gateway-northbound/data-cache-page',
    method: 'get',
    params: query
  })
}

export function getNorthboundDataCacheById(id) {
  return request({
    url: '/gateway-northbound/data-cache/' + id,
    method: 'get'
  })
}

export function getNorthboundAlarmCache(query) {
  return request({
    url: '/gateway-northbound/alarm-cache-page',
    method: 'get',
    params: query
  })
}

export function updateNorthbound(data) {
  return request({
    url: '/gateway-northbound',
    method: 'put',
    data
  })
}

export function updateNorthboundDataElement(data) {
  return request({
    url: '/gateway-northbound/dataElement',
    method: 'put',
    data
  })
}

export function createNorthbound(data) {
  return request({
    url: '/gateway-northbound',
    method: 'post',
    data
  })
}

export function testNorthbound(data) {
  return request({
    url: '/gateway-northbound/test',
    method: 'post',
    data
  })
}

export function deleteNorthbound(ids) {
  return request({
    url: '/gateway-northbound',
    method: 'delete',
    data: ids
  })
}

export function updateDataByNorthbound(nid, uploadData) {
  return request({
    url: '/gateway-northbound/data-by-northboundid?nid=' + nid + '&uploadData=' + uploadData,
    method: 'put'
  })
}

export function updateAlarmByNorthbound(nid, uploadAlarm) {
  return request({
    url: '/gateway-northbound/alarm-by-northboundid?nid=' + nid + '&uploadAlarm=' + uploadAlarm,
    method: 'put'
  })
}

export function updateDataByEquipment(nid, eid, uploadData) {
  return request({
    url: '/gateway-northbound/data-by-equipid?nid=' + nid + '&eid=' + eid + '&uploadData=' + uploadData,
    method: 'put'
  })
}

export function updateAlarmByEquipment(nid, eid, uploadAlarm) {
  return request({
    url: '/gateway-northbound/alarm-by-equipid?nid=' + nid + '&eid=' + eid + '&uploadAlarm=' + uploadAlarm,
    method: 'put'
  })
}

export function deleteDataCache(ids) {
  return request({
    url: '/gateway-northbound/data-cache',
    method: 'delete',
    data: ids
  })
}

export function deleteDataCacheAll(nid) {
  if (nid) {
    return request({
      url: '/gateway-northbound/data-cache-all?nid=' + nid,
      method: 'delete'
    })
  } else {
    return request({
      url: '/gateway-northbound/data-cache-all',
      method: 'delete'
    })
  }
}

export function deleteAlarmCache(ids) {
  return request({
    url: '/gateway-northbound/alarm-cache',
    method: 'delete',
    data: ids
  })
}

export function deleteAlarmCacheAll(nid) {
  if (nid) {
    return request({
      url: '/gateway-northbound/alarm-cache-all?nid=' + nid,
      method: 'delete'
    })
  } else {
    return request({
      url: '/gateway-northbound/alarm-cache-all',
      method: 'delete'
    })
  }
}

