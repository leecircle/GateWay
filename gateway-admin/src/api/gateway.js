import request from '@/utils/request'

export function getStatistics() {
  return request({
    url: '/gateway/statistics',
    method: 'get'
  })
}

export function getGateway() {
  return request({
    url: '/gateway',
    method: 'get'
  })
}

export function updateGateway(data) {
  return request({
    url: '/gateway',
    method: 'put',
    data
  })
}

export function applyConfig() {
  return request({
    url: '/config/apply',
    method: 'post'
  })
}

export function applyConfigToNorth() {
  return request({
    url: '/config/apply-to-north',
    method: 'post'
  })
}

export function applyConfigToSouth() {
  return request({
    url: '/config/apply-to-south',
    method: 'post'
  })
}
