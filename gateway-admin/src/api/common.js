import request from '@/utils/request'

export function getUserPagingList(query) {
  return request({
    url: '/api/services/app/CommonLookup/GetUserPagingListAsync',
    method: 'get',
    params: query
  })
}

export function getRoles() {
  return request({
    url: '/api/services/app/CommonLookup/GetRolesAsync',
    method: 'get'
  })
}

export function getModuleTypes() {
  return request({
    url: '/api/services/app/CommonLookup/GetModuleTypes',
    method: 'get'
  })
}
