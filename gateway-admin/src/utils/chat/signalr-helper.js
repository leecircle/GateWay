/* eslint-disable */
import abp from 'abp'
import { appConsts } from '@/utils/app-consts'
import { getCookieValue } from '@/utils/utils'

export function initSignalR(callback) {

    const encryptedAuthToken = getCookieValue(appConsts.authorization.encrptedAuthTokenName);

    abp.signalr = {
        autoConnect: true,
        connect: undefined,
        hubs: undefined,
        qs: appConsts.authorization.encrptedAuthTokenName + '=' + encodeURIComponent(encryptedAuthToken),
        remoteServiceBaseUrl: process.env.VUE_APP_BASE_API,
        startConnection: undefined,
        url: '/signalr'
    };

    import("@/utils/chat/abp.signalr-client").then(() => {
        callback();
    });
}
