const pattern = {
  ip: /^[1-9](\d{1,2})?\.(0|([1-9](\d{1,2})?))\.(0|([1-9](\d{1,2})?))\.(0|([1-9](\d{1,2})?))+$/, // ip地址
  rule01: /^(0|[1-9][0-9]*)$/, // 大于等于0的整数
  rule02: /^([1-9][0-9]*)$/ // 大于0的整数
};

export default pattern
