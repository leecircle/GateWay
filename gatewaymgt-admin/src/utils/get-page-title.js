import defaultSettings from '@/settings'
import i18n from '@/lang'
import abp from 'abp'

export default function getPageTitle(key) {
  const title = abp.setting.get('Site.SiteName') || defaultSettings.title || '家长知否'
  const hasKey = i18n.te(`route.${key}`)
  if (hasKey) {
    const pageName = i18n.t(`route.${key}`)
    return `${pageName} - ${title}`
  }
  return `${title}`
}
