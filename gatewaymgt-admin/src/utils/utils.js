import abp from 'abp'

export function getCookieValue(key) {
  return abp.utils.getCookieValue(key)
}

export function setCookieValue(key, value, expireDate, path) {  
  abp.utils.setCookieValue(key, value, expireDate, path)
}

export function deleteCookie(key, path) {
  abp.utils.deleteCookie(key, path)
}
