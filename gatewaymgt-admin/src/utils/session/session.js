import abp from 'abp'

export var session = {
  userId: abp.session.userId,
  tenantId: abp.session.tenantId,
  impersonatorUserId: abp.session.impersonatorUserId,
  impersonatorTenantId: abp.session.impersonatorTenantId,
  multiTenancySide: abp.multiTenancy.multiTenancySide
}
