// 波特率
export const btsOptions = [{
  value: 4800,
  label: '4800bps'
}, {
  value: 9600,
  label: '9600bps'
}, {
  value: 12800,
  label: '12800bps'
}, {
  value: 19200,
  label: '19200bps'
}, {
  value: 38400,
  label: '38400bps'
}, {
  value: 115200,
  label: '115200bps'
}];

// 流控制
export const flowControlOptions = [{
  value: 0,
  label: '无'
}, {
  value: 1,
  label: 'CTS/RTS'
}, {
  value: 2,
  label: 'DTR/DSR'
}];

// 数据位
export const dataBitsOptions = [{
  value: 6,
  label: '6位'
}, {
  value: 7,
  label: '7位'
}, {
  value: 8,
  label: '8位'
}];

// 停止位
export const stopBitsOptions = [{
  value: 1,
  label: '1位'
}, {
  value: 2,
  label: '2位'
}];

// 校验位
export const parityOptions = [{
  value: 0,
  label: '无'
}, {
  value: 1,
  label: '奇校验'
}, {
  value: 2,
  label: '偶校验'
}];

// 单次数据扫描周期
export const scanTimerOptions = [{
  value: 1,
  label: '1秒'
}, {
  value: 2,
  label: '2秒'
}, {
  value: 3,
  label: '3秒'
}, {
  value: 4,
  label: '4秒'
}, {
  value: 5,
  label: '5秒'
}];

// 单次数据扫描超时
export const scanTimeoutOptions = [{
  value: 200,
  label: '200毫秒'
}, {
  value: 500,
  label: '500毫秒'
}, {
  value: 1000,
  label: '1000毫秒'
}, {
  value: 2000,
  label: '2000毫秒'
}, {
  value: 5000,
  label: '5000毫秒'
}];

// 单次数据扫描重试
export const retryOptions = [{
  value: 1,
  label: '1次'
}, {
  value: 2,
  label: '2次'
}, {
  value: 5,
  label: '5次'
}];

// 故障状态扫描周期
export const faultScanTimerOptions = [{
  value: 30,
  label: '30秒'
}, {
  value: 60,
  label: '60秒'
}, {
  value: 300,
  label: '5分钟'
}, {
  value: 600,
  label: '10分钟'
}];

// 故障数据处理
export const faultDataHandleOptions = [{
  value: 1,
  label: '标记质量戳'
}, {
  value: 2,
  label: '清零'
}, {
  value: 3,
  label: '保留最后采集数'
}];

// 通道类型
export const physicalTypeOptions = [{
  value: 1,
  label: 'DCS-COM'
}, {
  value: 2,
  label: 'ModbusRTU-主站'
}, {
  value: 3,
  label: 'ModbusRTU-从站'
}, {
  value: 4,
  label: 'ModbusTCP-主站'
}, {
  value: 5,
  label: 'ModbusTCP-从站'
}, {
  value: 6,
  label: 'DCS-TCP'
}];

// 数据点类型
export const dataEleTypeOptions = [{
  value: 1,
  label: '模拟量'
}, {
  value: 2,
  label: '开关量'
}, {
  value: 3,
  label: '其他'
}];

// MODBUS解析数据类型
export const modbusDataTypeOptions = [{
  value: 1,
  label: '有符号整型 AB'
}, {
  value: 2,
  label: '无符号整型 AB'
}, {
  value: 3,
  label: '字符串'
}, {
  value: 4,
  label: '位'
}, {
  value: 5,
  label: '有符号长整型 AB CD'
}, {
  value: 6,
  label: '有符号长整型 CD AB'
}, {
  value: 7,
  label: '有符号长整型 BA DC'
}, {
  value: 8,
  label: '有符号长整型 DC BA'
}, {
  value: 9,
  label: '单精度浮点型 AB CD'
}, {
  value: 10,
  label: '单精度浮点型 CD AB'
}, {
  value: 11,
  label: '单精度浮点型 BA DC'
}, {
  value: 12,
  label: '单精度浮点型 DC BA'
}, {
  value: 13,
  label: '双精度浮点型 AB CD EF GH'
}, {
  value: 14,
  label: '双精度浮点型 GH EF CD AB'
}, {
  value: 15,
  label: '双精度浮点型 BA DC FE HG'
}, {
  value: 16,
  label: '双精度浮点型 HG FE DC BA'
}, {
  value: 17,
  label: '有符号整型 BA'
}, {
  value: 18,
  label: '无符号整型 BA'
}, {
  value: 19,
  label: '无符号长整型 AB CD'
}, {
  value: 20,
  label: '无符号长整型 CD AB'
}, {
  value: 21,
  label: '无符号长整型 BA DC'
}, {
  value: 22,
  label: '无符号长整型 DC BA'
}, {
  value: 23,
  label: '单字节有符号整型 A*'
}, {
  value: 24,
  label: '单字节有符号整型 *B'
}, {
  value: 25,
  label: '单字节无符号整型 A*'
}, {
  value: 26,
  label: '单字节无符号整型 *B'
}, {
  value: 27,
  label: '有符号BCD码 AB'
}, {
  value: 28,
  label: '无符号BCD码 AB'
}, {
  value: 29,
  label: '有符号BCD码 BA'
}, {
  value: 30,
  label: '无符号BCD码 BA'
}];

// 日志类型
export const logLevelOptions = [{
  value: 'ERROR',
  label: 'ERROR'
}, {
  value: 'INFO',
  label: 'INFO'
}, {
  value: 'WARN',
  label: 'WARN'
}
// , {
//   value: 'DEBUG',
//   label: 'DEBUG'
// }
];

// 告警日志类型
export const alarmLogLevelOptions = [{
  value: 'clear',
  label: 'clear'
}, {
  value: 'minor',
  label: 'minor'
}, {
  value: 'major',
  label: 'major'
}, {
  value: 'critical',
  label: 'critical'
}];

// 告警分类
export const alarmCategoryOptions = [{
  value: 'LL',
  label: '低低报'
}, {
  value: 'L',
  label: '低报'
}, {
  value: 'H',
  label: '高报'
}, {
  value: 'HH',
  label: '高高报'
}];

// 上传网络分类
export const linkTypeOptions = [{
  value: 1,
  label: '互联网'
}, {
  value: 2,
  label: '专线网络'
}, {
  value: 3,
  label: 'VPN网络'
}, {
  value: 4,
  label: '企业内网'
}];

// 最多存储天数
export const maintainDaysOptions = [{
  value: 30,
  label: '30天'
}, {
  value: 90,
  label: '90天'
}, {
  value: 180,
  label: '180天'
}];

// 日志告警阈值
export const alarmPercentageThresholdOptions = [{
  value: 70,
  label: '70%'
}, {
  value: 85,
  label: '85%'
}, {
  value: 90,
  label: '90%'
}];
