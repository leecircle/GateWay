/* eslint-disable */
import i18n from '@/lang'
import moment from 'moment';
export const pickerOptions = {
    shortcuts: [
        {
            text: i18n.t("helpers.today"),
            onClick(picker) {
                const start = moment().startOf('day').toDate();
                const end = moment().endOf('day').toDate();
                picker.$emit("pick", [start, end]);
            }
        },
        {
            text: i18n.t("helpers.yesterday"),
            onClick(picker) {
                const start = moment().subtract(1, 'days').startOf('day').toDate();
                const end = moment().subtract(1, 'days').endOf('day').toDate();
                picker.$emit("pick", [start, end]);
            }
        },
        {
            text: i18n.t("helpers.lastWeek"),
            onClick(picker) {
                const start = moment().subtract(1, 'weeks').startOf('day').toDate();
                const end = moment().endOf('day').toDate();
                picker.$emit("pick", [start, end]);
            }
        },
        {
            text: i18n.t("helpers.lastMonth"),
            onClick(picker) {
                const start = moment().subtract(1, 'months').startOf('day').toDate();
                const end = moment().endOf('day').toDate();
                picker.$emit("pick", [start, end]);
            }
        },
        {
            text: i18n.t("helpers.lastQuarter"),
            onClick(picker) {
                const start = moment().subtract(1, 'quarter').startOf('day').toDate();
                const end = moment().endOf('day').toDate();
                picker.$emit("pick", [start, end]);
            }
        },
        {
            text: i18n.t("helpers.lastHalfYear"),
            onClick(picker) {
                const start = moment().subtract(6, 'months').startOf('day').toDate();
                const end = moment().endOf('day').toDate();
                picker.$emit("pick", [start, end]);
            }
        },
        {
            text: i18n.t("helpers.lastYear"),
            onClick(picker) {
                const start = moment().subtract(1, 'years').startOf('day').toDate();
                const end = moment().endOf('day').toDate();
                picker.$emit("pick", [start, end]);
            }
        }
    ]
}
