export function returnUrlToRedirectUri(returnUrl) {
    if (!returnUrl) {        
        return returnUrl;
    }
    const redirectUri = returnUrl.replace(new RegExp('\\?', 'g'), '_:_').replace(new RegExp('&', 'g'), '_~_');
    const result = redirectUri;
    return result;
}

export function redirectUriToReturnUrl(redirectUri) {
    if (!redirectUri) {        
        return redirectUri;
    }
    const returnUrl = redirectUri.replace(/_:_/g, '?').replace(/_~_/g, '&');
    const result = returnUrl;
    return result;
}
