import abp from 'abp'

export function getToken() {
  return abp.auth.getToken()
}

export function getTokenCookieName() {
  return abp.auth.tokenCookieName
}

export function setToken(authToken, expireDate) {
  abp.auth.setToken(authToken, expireDate)
}

export function clearToken() {
  abp.auth.clearToken()
}
