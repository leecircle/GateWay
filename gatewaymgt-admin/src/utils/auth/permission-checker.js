import abp from 'abp'

export function isGranted(permissionName) {
  return abp.auth.isGranted(permissionName)
}

export function isAnyGranted(permissionNames) {
  return abp.auth.isAnyGranted(...permissionNames)
}
