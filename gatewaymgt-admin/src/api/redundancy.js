import request from '@/utils/request'

export function getRedundancy() {
  return request({
    url: '/api/gateway-redundancy',
    method: 'get'
  })
}
export function getRedundancyById(id) {
  return request({
    url: '/api/gateway-redundancy/' + id,
    method: 'get'
  })
}

export function deleteRedundancy(keys) {
  return request({
    url: '/api/gateway-redundancy',
    method: 'delete',
    data: keys
  })
}

export function updateRedundancy(data) {
  return request({
    url: '/api/gateway-redundancy',
    method: 'put',
    data
  })
}

export function createRedundancy(data) {
  return request({
    url: '/api/gateway-redundancy',
    method: 'post',
    data
  })
}
