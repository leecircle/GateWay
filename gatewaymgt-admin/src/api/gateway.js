import request from '@/utils/request'

export function getGatewayTree() {
  return request({
    url: '/api/gateway/tree',
    method: 'get'
  })
}

export function getGatewayShort() {
  return request({
    url: '/api/gateway/short',
    method: 'get'
  })
}

export function getGatewayShortest() {
  return request({
    url: '/api/gateway/shortest',
    method: 'get'
  })
}

export function getGatewayStatistics(gatewayId) {
  return request({
    url: '/api/gateway/statistics/' + gatewayId,
    method: 'get'
  })
}

export function getGateway(gatewayId) {
  return request({
    url: '/api/gateway/' + gatewayId,
    method: 'get'
  })
}

export function updateGateway(data) {
  return request({
    url: '/api/gateway',
    method: 'put',
    data
  })
}

export function applyPortConfig(gatewayId) {
  return request({
    url: '/api/config/port-conf/' + gatewayId,
    method: 'post'
  })
}

export function applySyncConfig(gatewayId) {
  return request({
    url: '/api/config/sync/' + gatewayId,
    method: 'post'
  })
}

export function applyConfigToNorth(gatewayId) {
  return request({
    url: '/api/config/north-conf/' + gatewayId,
    method: 'post'
  })
}

export function applyConfigToSouth() {
  return request({
    url: '/api/config/apply-to-south',
    method: 'post'
  })
}

export function getMgtDataCache(query) {
  return request({
    url: '/api/mgt-data-cache',
    method: 'get',
    params: query
  })
}

export function getMgtAlarmCache(query) {
  return request({
    url: '/api/mgt-alarm-cache',
    method: 'get',
    params: query
  })
}

export function deleteMgtDataCache(ids) {
  return request({
    url: '/api/mgt-data-cache/ids',
    method: 'delete',
    data: ids
  })
}

export function deleteAllMgtDataCache() {
  return request({
    url: '/api/mgt-data-cache/all',
    method: 'delete'
  })
}

export function deleteMgtDataCacheByGatewayId(gatewayId) {
  return request({
    url: '/api/mgt-data-cache/' + gatewayId,
    method: 'delete'
  })
}

export function deleteMgtAlarmCache(ids) {
  return request({
    url: '/api/mgt-alarm-cache/ids',
    method: 'delete',
    data: ids
  })
}

export function deleteAllMgtAlarmCache() {
  return request({
    url: '/api/mgt-alarm-cache/all',
    method: 'delete'
  })
}

export function deleteMgtAlarmCacheByGatewayId(gatewayId) {
  return request({
    url: '/api/mgt-alarm-cache/' + gatewayId,
    method: 'delete'
  })
}

export function startGateway(gatewayId) {
  return request({
    url: '/api/gateway/start-producer/' + gatewayId,
    method: 'put'
  })
}

export function stopGateway(gatewayId) {
  return request({
    url: '/api/gateway/stop-producer/' + gatewayId,
    method: 'put'
  })
}

export function deleteGateway(gatewayId) {
  return request({
    url: '/api/gateway/' + gatewayId,
    method: 'delete'
  })
}

export function exportGateway() {
  return request({
    url: '/api/export/gateways',
    method: 'get',
    responseType: 'blob'
  })
}
