import request from '@/utils/request'

export function getRolePagingList(query) {
  return request({
    url: '/api/roles/pageable',
    method: 'get',
    params: query
  })
}

export function getRoleList() {
  return request({
    url: '/api/roles',
    method: 'get'
  })
}
export function getRoleForEdit(id) {
  return request({
    url: '/api/roles/' + id,
    method: 'get'
  })
}
export function createRole(data) {
  return request({
    url: '/api/roles',
    method: 'post',
    data
  })
}
export function updateRole(id, data) {
  return request({
    url: '/api/roles/' + id,
    method: 'put',
    data
  })
}
export function deleteRole(id) {
  return request({
    url: '/api/roles/' + id,
    method: 'delete'
  })
}

export function updateRolePermissions(id, data) {
  return request({
    url: '/api/roles/' + id + '/authorize',
    method: 'put',
    data
  })
}
