import request from '@/utils/request'

export function getAllEquipment() {
  return request({
    url: '/api/equipment/all',
    method: 'get'
  })
}
export function getAllEquipmentShort(gatewayId) {
  return request({
    url: '/api/equipment/all-short?gatewayId=' + gatewayId,
    method: 'get'
  })
}
export function getEquipmentByPortId(gatewayId, portId) {
  return request({
    url: '/api/equipment/all-by-portId?gatewayId=' + gatewayId + '&portId=' + portId,
    method: 'get'
  })
}
export function getEquipmentById(id) {
  return request({
    url: '/api/equipment?id=' + id,
    method: 'get'
  })
}

export function updateEquipment(data) {
  return request({
    url: '/api/equipment',
    method: 'put',
    data
  })
}

export function createEquipment(data) {
  return request({
    url: '/api/equipment',
    method: 'post',
    data
  })
}

export function deleteEquipment(ids) {
  return request({
    url: '/api/equipment',
    method: 'delete',
    data: ids
  })
}
