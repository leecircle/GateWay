import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/auth/login',
    method: 'post',
    data
  })
}

export function updatePass(data) {
  return request({
    url: '/api/users/updatePass',
    method: 'post',
    data
  })
}

export function weixinComponentUserInfoCallback(data) {
  return request({
    url: '/ngexpress/api/ngexpress/v1/wxCompOpenAuthenticate',
    method: 'post',
    params: data
  })
}
export function logout() {
  return request({
    url: '/ngexpress/api/TokenAuth/LogOut',
    method: 'get'
  })
}

export function getAll() {
  return request({
    url: '/auth/getall',
    method: 'get'
  })
}

export function getVerifyCode() {
  return request({
    url: '/auth/verifyCode',
    method: 'get',
    responseType: 'blob'
  })
}
