import request from '@/utils/request'

export function getShortDataElementByPortId(portId) {
  return request({
    url: '/api/dataElement/short?portId=' + portId,
    method: 'get'
  })
}
export function exportDataElement(gatewayId, equipmentId) {
  return request({
    url: '/api/export/dataElement/' + gatewayId + '/' + equipmentId,
    method: 'get',
    responseType: 'blob'
  })
}

export function importDataElement(gatewayId, physicalPortId, equipmentId, param) {
  return request({
    url: '/api/import/dataElement/' + gatewayId + '/' + physicalPortId + '/' + equipmentId,
    method: 'post',
    data: param
  })
}

export function getShortPageDataElement(query) {
  return request({
    url: '/api/dataElement/shortpage',
    method: 'get',
    params: query
  })
}
export function getFullDataElementByPortId(portId) {
  return request({
    url: '/api/dataElement/full?portId=' + portId,
    method: 'get'
  })
}

export function getFullPageDataElement(query) {
  return request({
    url: '/api/dataElement/fullpage',
    method: 'get',
    params: query
  })
}

export function getValueDataElementByPortId(portId) {
  return request({
    url: '/api/dataElement/value?portId=' + portId,
    method: 'get'
  })
}

export function getValuePageDataElement(query) {
  return request({
    url: '/api/dataElement/value',
    method: 'get',
    params: query
  })
}

export function getValueRealtimePageDataElement(query) {
  return request({
    url: '/api/dataElement/value-realtime/' + query.gatewayId,
    method: 'get',
    params: query
  })
}

export function getDataElementById(id) {
  return request({
    url: '/api/dataElement?id=' + id,
    method: 'get'
  })
}

export function createDataElement(data) {
  return request({
    url: '/api/dataElement/create',
    method: 'post',
    data
  })
}

export function updateDataElement(data) {
  return request({
    url: '/api/dataElement/update',
    method: 'put',
    data
  })
}

export function deleteDataElement(ids) {
  return request({
    url: '/api/dataElement',
    method: 'delete',
    data: ids
  })
}
