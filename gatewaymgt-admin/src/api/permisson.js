import request from '@/utils/request'

export function getAllPermissions() {
  return request({
    url: '/api/auths',
    method: 'get'
  })
}

export function getAuthorityInfoUsingGET(id) {
  return request({
    url: '/api/auths/' + id,
    method: 'get'
  })
}

export function createAuthorityInfoUsingPOST(data) {
  return request({
    url: '/api/auths',
    method: 'post',
    data
  })
}

export function updateAuthorityInfoUsingPUT(data) {
  return request({
    url: '/api/auths/' + data.id,
    method: 'put',
    data
  })
}

export function deleteAuthorityInfoUsingDELETE(id) {
  return request({
    url: '/api/auths/' + id,
    method: 'delete'
  })
}
