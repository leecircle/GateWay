import request from '@/utils/request'

export function getDashboard() {
  return request({
    url: '/api/dashboard',
    method: 'get'
  })
}
