import request from '@/utils/request'
import qs from 'qs'

export function getUserPagingList(query) {
  return request({
    url: '/api/users',
    method: 'get',
    params: query
  })
}

export function getUserForEdit(id) {
  return request({
    url: '/api/users/' + id,
    method: 'get'
  })
}

export function createUser(data) {
  return request({
    url: '/api/users',
    method: 'post',
    data
  })
}
export function updateUser(data) {
  return request({
    url: '/api/users',
    method: 'put',
    data
  })
}

export function deleteUsers(ids) {
  return request({
    url: '/api/users',
    method: 'delete',
    data: ids
  })
}

export function resetPassPassword(id) {
  return request({
    url: '/api/users/resetPass/' + id,
    method: 'post'
  })
}

export function changePassword(data) {
  return request({
    url: '/api/services/app/UserAdmin/ChangePasswordAsync',
    method: 'post',
    data
  })
}

export function getRolesName(id) {
  return request({
    url: '/api/services/app/UserAdmin/GetRolesNameAsync',
    method: 'get',
    params: {
      id
    }
  })
}

export function setRoles(data) {
  return request({
    url: '/api/services/app/UserAdmin/SetRolesAsync',
    method: 'post',
    data
  })
}
export function getUserPermissions(id) {
  return request({
    url: '/api/services/app/UserAdmin/GetUserPermissionsAsync',
    method: 'get',
    params: {
      id
    }
  })
}
export function updateUserPermissions(data) {
  return request({
    url: '/api/services/app/UserAdmin/UpdateUserPermissionsAsync',
    method: 'put',
    data
  })
}
export function resetAllPermissions(data) {
  return request({
    url: '/api/services/app/UserAdmin/ResetAllPermissionsAsync',
    method: 'post',
    data
  })
}

export function getPasswordComplexitySetting() {
  return request({
    url: '/api/services/app/User/GetPasswordComplexitySetting',
    method: 'get'
  })
}
