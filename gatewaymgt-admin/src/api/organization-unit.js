import request from '@/utils/request'

export function getOrganizationUnits() {
  return request({
    url: '/api/services/app/OrganizationUnit/GetOrganizationUnitsAsync',
    method: 'get'
  })
}

export function getOrganizationUnitsWithLevel() {
  return request({
    url: '/api/services/app/OrganizationUnit/GetOrganizationUnitsWithLevelAsync',
    method: 'get'
  })
}

export function getOrganizationUnit(id) {
  return request({
    url: '/api/services/app/OrganizationUnit/GetOrganizationUnitAsync',
    method: 'get',
    params: { id }
  })
}

export function createOrganizationUnit(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/CreateOrganizationUnitAsync',
    method: 'post',
    data
  })
}

export function updateOrganizationUnit(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/UpdateOrganizationUnitAsync',
    method: 'put',
    data
  })
}

export function moveOrganizationUnit(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/MoveOrganizationUnitAsync',
    method: 'post',
    data
  })
}

export function deleteOrganizationUnit(id) {
  return request({
    url: '/api/services/app/OrganizationUnit/DeleteOrganizationUnitAsync',
    method: 'delete',
    params: { id }
  })
}

export function getOrganizationUnitUserPagingList(query) {
  return request({
    url: '/api/services/app/OrganizationUnit/GetOrganizationUnitUserPagingListAsync',
    method: 'get',
    params: query
  })
}

export function removeUserFromOrganizationUnit(userId, organizationUnitId) {
  return request({
    url: '/api/services/app/OrganizationUnit/RemoveUserFromOrganizationUnitAsync',
    method: 'delete',
    params: { userId, organizationUnitId }
  })
}

export function setUserToOrganizationUnitBoss(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/SetUserToOrganizationUnitBossAsync',
    method: 'post',
    data
  })
}

export function getUserPagingListForOu(query) {
  return request({
    url: '/api/services/app/OrganizationUnit/GetUserPagingListForOuAsync',
    method: 'get',
    params: query
  })
}

export function addUserToOrganizationUnit(data) {
  return request({
    url: '/api/services/app/OrganizationUnit/AddUserToOrganizationUnitAsync',
    method: 'post',
    data
  })
}
