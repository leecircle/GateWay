import request from '@/utils/request'

export function getVersionLatest() {
  return request({
    url: '/api/version/latest',
    method: 'get'
  })
}

export function importVersionDc(param) {
  return request({
    url: '/api/version/importdc',
    method: 'post',
    data: param
  })
}
export function importVersionDg(param) {
  return request({
    url: '/api/version/importdg',
    method: 'post',
    data: param
  })
}

export function getVersionAlls(query) {
  return request({
    url: '/api/version/alls',
    method: 'get',
    params: query
  })
}

export function updateVersionAll(gatewayId) {
  return request({
    url: '/api/version/all/' + gatewayId,
    method: 'post'
  })
}

export function updateVersionDg(gatewayId) {
  return request({
    url: '/api/version/dg/' + gatewayId,
    method: 'post'
  })
}

export function updateVersionAlls(ids) {
  return request({
    url: '/api/version/alls',
    method: 'post',
    data: ids
  })
}

export function updateVersionDgs(ids) {
  return request({
    url: '/api/version/dgs',
    method: 'post',
    data: ids
  })
}
