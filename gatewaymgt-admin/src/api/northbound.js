import request from '@/utils/request'

export function getAllNorthbound(gatewayId) {
  return request({
    url: '/api/gateway-northbound/full/' + gatewayId,
    method: 'get'
  })
}

export function getNorthboundWithEquipment(gatewayId) {
  return request({
    url: '/api/gateway-northbound/full-with-equipment/' + gatewayId,
    method: 'get'
  })
}

export function getShortNorthbound(gatewayId) {
  return request({
    url: '/api/gateway-northbound/short/' + gatewayId,
    method: 'get'
  })
}

export function getNorthboundEquipment() {
  return request({
    url: '/api/gateway-northbound/equip',
    method: 'get'
  })
}

export function getNorthboundDataElement(query) {
  return request({
    url: '/api/gateway-northbound/dataElement',
    method: 'get',
    params: query
  })
}
export function getNorthboundCache(gatewayId) {
  return request({
    url: '/api/gateway-northbound/cache/' + gatewayId,
    method: 'get'
  })
}
export function getNorthboundDataCache(query) {
  return request({
    url: '/api/gateway-northbound/data-cache-page',
    method: 'get',
    params: query
  })
}

export function getNorthboundDataCacheById(id) {
  return request({
    url: '/api/mgt-data-cache/' + id,
    method: 'get'
  })
}
export function getNorthboundAlarmCache(query) {
  return request({
    url: '/api/gateway-northbound/alarm-cache-page',
    method: 'get',
    params: query
  })
}

export function updateNorthbound(data) {
  return request({
    url: '/api/gateway-northbound',
    method: 'put',
    data
  })
}

export function updateNorthboundDataElement(data) {
  return request({
    url: '/api/gateway-northbound/dataElement',
    method: 'put',
    data
  })
}

export function createNorthbound(data) {
  return request({
    url: '/api/gateway-northbound',
    method: 'post',
    data
  })
}

export function deleteNorthbound(ids) {
  return request({
    url: '/api/gateway-northbound',
    method: 'delete',
    data: ids
  })
}

export function updateDataByNorthbound(gatewayId, nid, uploadData) {
  return request({
    url: '/api/gateway-northbound/data-by-northboundid/' + gatewayId + '?nid=' + nid + '&uploadData=' + uploadData,
    method: 'put'
  })
}

export function updateAlarmByNorthbound(gatewayId, nid, uploadAlarm) {
  return request({
    url: '/api/gateway-northbound/alarm-by-northboundid/' + gatewayId + '?nid=' + nid + '&uploadAlarm=' + uploadAlarm,
    method: 'put'
  })
}

export function updateDataByEquipment(gatewayId, nid, eid, uploadData) {
  return request({
    url: '/api/gateway-northbound/data-by-equipid/' + gatewayId + '?nid=' + nid + '&eid=' + eid + '&uploadData=' + uploadData,
    method: 'put'
  })
}

export function updateAlarmByEquipment(gatewayId, nid, eid, uploadAlarm) {
  return request({
    url: '/api/gateway-northbound/alarm-by-equipid/' + gatewayId + '?nid=' + nid + '&eid=' + eid + '&uploadAlarm=' + uploadAlarm,
    method: 'put'
  })
}

export function deleteDataCache(gatewayId, nid) {
  return request({
    url: '/api/gateway-northbound/data-cache-all/' + gatewayId + '?nid=' + nid,
    method: 'delete'
  })
}

export function deleteAlarmCache(gatewayId, nid) {
  return request({
    url: '/api/gateway-northbound/alarm-cache-all/' + gatewayId + '?nid=' + nid,
    method: 'delete'
  })
}
