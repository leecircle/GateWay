import request from '@/utils/request'

export function getAuditLogPagingList(query) {
  return request({
    url: '/api/services/app/AuditLogAdmin/GetAuditLogPagingListAsync',
    method: 'get',
    params: query
  })
}
export function getAuditLog(id) {
  return request({
    url: '/api/services/app/AuditLogAdmin/GetAuditLogAsync',
    method: 'get',
    params: { id }
  })
}
