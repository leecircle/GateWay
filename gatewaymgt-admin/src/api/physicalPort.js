import request from '@/utils/request'

export function getShortPhysicalPort() {
  return request({
    url: '/api/physicalport/short',
    method: 'get'
  })
}
export function getFullPhysicalPort(gatewayId) {
  return request({
    url: '/api/physicalport/full?gatewayId=' + gatewayId,
    method: 'get'
  })
}
export function getShortPhysicalPortWithEquipment(gatewayId) {
  return request({
    url: '/api/physicalport/short-with-equipment?gatewayId=' + gatewayId,
    method: 'get'
  })
}

export function getPhysicalPortById(id) {
  return request({
    url: '/api/physicalport?id=' + id,
    method: 'get'
  })
}
export function getPortIdList(gatewayId) {
  return request({
    url: '/api/physicalport/getPortIdList?gatewayId=' + gatewayId,
    method: 'get'
  })
}

export function getPortTypeById(id) {
  return request({
    url: '/api/physicalport/getPortTypeById?id=' + id,
    method: 'get'
  })
}

export function updatePhysicalPort(data) {
  return request({
    url: '/api/physicalport',
    method: 'put',
    data
  })
}

export function updatePhysicalPortStatus(data) {
  return request({
    url: '/api/physicalport/updateStatus',
    method: 'post',
    data
  })
}

export function createPhysicalPort(data) {
  return request({
    url: '/api/physicalport/create',
    method: 'post',
    data
  })
}

export function deletePhysicalPort(ids) {
  return request({
    url: '/api/physicalport',
    method: 'delete',
    data: ids
  })
}

export function exportPhysicalPort(gatewayId) {
  return request({
    url: '/api/export/physicalPort/' + gatewayId,
    method: 'get',
    responseType: 'blob'
  })
}

export function importPhysicalPort(gatewayId, param) {
  return request({
    url: '/api/import/physicalPort/' + gatewayId,
    method: 'post',
    data: param
  })
}
