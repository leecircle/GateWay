import request from '@/utils/request'

export function getDataCenter() {
  return request({
    url: '/api/data-center',
    method: 'get'
  })
}

export function updateDataCenter(data) {
  return request({
    url: '/api/data-center',
    method: 'put',
    data
  })
}
