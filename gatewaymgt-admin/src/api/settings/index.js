import request from '@/utils/request'

export function changeLanguage(data) {
    return request({
        url: '/api/services/app/Settings/ChangeLanguageAsync',
        method: 'post',
        data
    })
}
export function getCachePagingList(query) {
    return request({
        url: '/api/services/app/Settings/GetCachePagingList',
        method: 'get',
        params: query
    })
}
export function clearCache(name) {
    return request({
        url: '/api/services/app/Settings/ClearCache',
        method: 'post',
        params: { name }
    })
}

export function setDatetime(time) {
  return request({
    url: '/sys/datetime',
    method: 'post',
    data: time
  })
}

export function mgtInit() {
  return request({
    url: '/api/sys/init',
    method: 'post'
  })
}

export function mgtReboot() {
  return request({
    url: '/api/sys/reboot',
    method: 'post'
  })
}

export function gatewayInit(gatewayId) {
  return request({
    url: '/api/sys/init/' + gatewayId,
    method: 'post'
  })
}

export function gatewayResetPass(gatewayId) {
  return request({
    url: '/api/sys/resetPass/' + gatewayId,
    method: 'post'
  })
}

export function gatewayReBoot(gatewayId) {
  return request({
    url: '/api/sys/reboot/' + gatewayId,
    method: 'post'
  })
}
