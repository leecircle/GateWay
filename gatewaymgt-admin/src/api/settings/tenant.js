import request from '@/utils/request'

export function getAllSettings() {
  return request({
    url: '/api/services/app/TenantSettings/GetAllSettings',
    method: 'get'
  })
}
export function updateAllSettings(data) {
  return request({
    url: '/api/services/app/TenantSettings/UpdateAllSettings',
    method: 'put',
    data
  })
}
