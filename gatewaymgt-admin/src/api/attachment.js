import request from '@/utils/request'

export function getMyPictureAttachmentPagingList(query) {
  return request({
    url: '/api/services/app/Attachment/GetMyPictureAttachmentPagingListAsync',
    method: 'get',
    params: query
  })
}
export function deleteAttachment(id) {
  return request({
    url: '/api/services/app/Attachment/DeleteAttachmentAsync',
    method: 'delete',
    params: { id }
  })
}
