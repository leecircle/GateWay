import request from '@/utils/request'

export function getlogsPage(query) {
  return request({
    url: '/api/logs',
    method: 'get',
    params: query
  })
}

export function getSecurityLogsPage(query) {
  return request({
    url: '/api/logs/security',
    method: 'get',
    params: query
  })
}

export function exportSecurityLogs(query) {
  return request({
    url: '/api/logs/export/security',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

export function deleteSecurityLog(ids) {
  return request({
    url: '/api/logs/portion-security',
    method: 'delete',
    data: ids
  })
}

export function deleteAllSecurityLog() {
  return request({
    url: '/api/logs/all-security',
    method: 'delete'
  })
}

// 告警
export function getAlarmLogsPage(query) {
  return request({
    url: '/api/logs/alarm',
    method: 'get',
    params: query
  })
}

export function exportAlarmLogs(query) {
  return request({
    url: '/api/logs/export/alarm',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

export function deleteAlarmLog(ids) {
  return request({
    url: '/api/logs/portion-alarm',
    method: 'delete',
    data: ids
  })
}

export function deleteAllAlarmLog() {
  return request({
    url: '/api/logs/all-alarm',
    method: 'delete'
  })
}

// 操作
export function getOperationLogsPage(query) {
  return request({
    url: '/api/logs/operation',
    method: 'get',
    params: query
  })
}

export function exportOperationLogs(query) {
  return request({
    url: '/api/logs/export/operation',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

export function deleteOperationLog(ids) {
  return request({
    url: '/api/logs/portion-operation',
    method: 'delete',
    data: ids
  })
}

export function deleteAllOperationLog() {
  return request({
    url: '/api/logs/all-operation',
    method: 'delete'
  })
}

// 状态
export function getStatusLogsPage(query) {
  return request({
    url: '/api/logs/status',
    method: 'get',
    params: query
  })
}

export function exportStatusLogs(query) {
  return request({
    url: '/api/logs/export/status',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

export function deleteStatusLog(ids) {
  return request({
    url: '/api/logs/portion-status',
    method: 'delete',
    data: ids
  })
}

export function deleteAllStatusLog() {
  return request({
    url: '/api/logs/all-status',
    method: 'delete'
  })
}

export function getLogSetting() {
  return request({
    url: '/api/log-setting',
    method: 'get'
  })
}

export function updateLogSetting(data) {
  return request({
    url: '/api/log-setting',
    method: 'put',
    data
  })
}
