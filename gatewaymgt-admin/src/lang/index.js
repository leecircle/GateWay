import Vue from 'vue'
import VueI18n from 'vue-i18n'
import elementEnLocale from 'element-ui/lib/locale/lang/en' // element-ui lang
import elementZhLocale from 'element-ui/lib/locale/lang/zh-CN'// element-ui lang
import enLocale from './en'
import zhLocale from './zh-Hans'

import { getCookieValue } from '@/utils/utils'

Vue.use(VueI18n)

export const LANGS = {
  'zh-Hans': {
    text: '简体中文',
    vue: 'zh-Hans',
    abp: 'zh-Hans',
    moment: 'zh-cn'
  },
  'en': {
    text: 'English',
    vue: 'en',
    abp: 'en',
    moment: 'en'
  }
}

const messages = {
  en: {
    ...enLocale,
    ...elementEnLocale
  },
  'zh-Hans': {
    ...zhLocale,
    ...elementZhLocale
  }
}

export function getLanguageItem(language) {  
  const langs = Object.keys(LANGS);
  const lang = langs.includes(language) ? language : langs[0];
  const item = LANGS[lang];
  return item
}

export function getLanguage() {
  const cookieLanguage = getCookieValue('Abp.Localization.CultureName')
  const language = cookieLanguage || navigator.language || navigator.browserLanguage;
  var item = getLanguageItem(language)

  return item.vue
}

const i18n = new VueI18n({
  // set locale
  // options: en | zh | es
  locale: getLanguage(),
  // set locale messages
  messages
})

export default i18n
