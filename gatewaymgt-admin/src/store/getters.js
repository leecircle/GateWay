const getters = {
  app: state => state.app.app,
  sidebar: state => state.app.sidebar,
  language: state => state.app.language,
  size: state => state.app.size,
  device: state => state.app.device,
  visitedViews: state => state.tagsView.visitedViews,
  cachedViews: state => state.tagsView.cachedViews,
  token: state => state.account.token,

  application: state => state.session.application,
  tenant: state => state.session.tenant,
  tenantId: state => state.session.tenantId,
  user: state => state.session.user,
  firstLogin: state => state.session.firstLogin,
  userId: state => state.session.userId,

  permission_routes: state => state.permission.routes,
  errorLogs: state => state.errorLog.logs
}
export default getters
