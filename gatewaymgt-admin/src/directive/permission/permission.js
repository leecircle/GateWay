import { isAnyGranted } from '@/utils/auth/permission-checker'

export default {
  inserted(el, binding, vnode) {
    const { value } = binding
    if (value && value instanceof Array && value.length > 0) {
      const permissionNames = value
      if (!isAnyGranted(permissionNames)) {
        el.parentNode && el.parentNode.removeChild(el)
      }
    } else {
      throw new Error(`need permissions! Like v-permission="['Admin.User','Admin.Role']"`)
    }
  }
}
