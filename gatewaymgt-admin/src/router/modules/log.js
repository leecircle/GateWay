/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const logRouter = {
  path: '/log',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  name: 'log',
  meta: {
    title: 'logService',
    icon: 'log',
    permissions: ['Admin.Log']
  },
  children: [
  {
    path: 'security',
    component: () => import('@/views/log/security'),
    name: 'security-log',
    meta: {
      title: 'securityLog',
      permissions: ['Admin.Log.Update']
    }
  },
  {
    path: 'alarm',
    component: () => import('@/views/log/alarm'),
    name: 'alarm-log',
    meta: {
      title: 'alarmLog'
    }
  },
  {
    path: 'operation',
    component: () => import('@/views/log/operation'),
    name: 'operation-log',
    meta: {
      title: 'operationLog'
    }
  },
  {
    path: 'status',
    component: () => import('@/views/log/status'),
    name: 'status-log',
    meta: {
      title: 'statusLog'
    }
  },
  {
    path: '',
    component: () => import('@/views/log/index'),
    name: 'log-config',
    meta: {
      title: 'logConfig',
      permissions: ['Admin.Log.Update']
    }
  },
  // {
  //   path: 'other',
  //   component: () => import('@/views/log/other'),
  //   name: 'other-log',
  //   meta: {
  //     title: 'otherLog'
  //   }
  // }
  ]
}
export default logRouter
