/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const scoreRouter = {
    path: '/scores',
    component: Layout,
    redirect: 'noRedirect',
    alwaysShow: true, // will always show the root menu
    name: 'scores',
    meta: {
      title: 'scores',
      icon: 'nested',
      permissions: ['Page']
    },
    children: [
      {
        path: '',
        component: () => import('@/views/scores'),
        name: 'score-list',
        meta: {
          title: 'scores',
          permissions: ['Page']
        }
      },
      {
        path: 'edit',
        component: () => import('@/views/scores/edit'),
        name: 'scores-edit',
        meta: {
          title: 'scoresEdit',
          permissions: ['Page']
        }
      },
      {
        path: 'detail/:id(\\d+)',
        hidden: true,
        component: () => import('@/views/scores/detail'),
        name: 'scores-detail',
        meta: {
          title: 'scoresDetail',
          activeMenu: '/scores',
          permissions: ['Page']
        }
      }
    ]
  }
export default scoreRouter
