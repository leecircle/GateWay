/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const expressRouter = {
  path: '/express',
  component: Layout,
  redirect: 'noRedirect',
  hidden: true,
  children: [
    {
      path: 'select',
      component: () => import('@/views/express/select'),
      name: 'expressSelect',
      meta: {
        title: 'expressSelect'
        // permissions: ['Admin.Setting.Tenant']
      }
    },
    {
      path: 'estimate',
      component: () => import('@/views/express/estimate'),
      name: 'expressEstimate',
      meta: {
        title: 'expressEstimate'
        // permissions: ['Admin.Setting.Tenant']
      }
    },
    {
      path: 'estimate',
      component: () => import('@/views/express/estimate'),
      name: 'expressEstimate',
      meta: {
        title: 'expressEstimate'
        // permissions: ['Admin.Setting.Tenant']
      }
    }
  ]
}
export default expressRouter
