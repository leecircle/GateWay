/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const uploadRouter = {
  path: '/upload',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  name: 'upload',
  meta: {
    title: 'upload',
    icon: 'shangchuan'
  },
  children: [{
    path: '',
    component: () => import('@/views/upload/index'),
    name: 'upload-config',
    meta: {
      title: 'uploadConfig'
    }
  },
    {
      path: 'dataCache',
      component: () => import('@/views/upload/data-cache'),
      name: 'data-cache',
      meta: {
        title: 'dataCache'
      }
    },
    {
      path: 'alarmCache',
      component: () => import('@/views/upload/alarm-cache'),
      name: 'alarm-cache',
      meta: {
        title: 'alarmCache'
      }
    }
  ]
}
export default uploadRouter
