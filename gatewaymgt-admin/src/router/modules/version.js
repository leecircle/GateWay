/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const versionRouter = {
  path: '/version',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  name: 'version',
  meta: {
    title: 'version',
    icon: 'version',
    permissions: ['Admin.Version']
  },
  children: [{
    path: '',
    component: () => import('@/views/version/index'),
    name: 'version-upload',
    meta: {
      title: 'versionUpload'
    }
  },
    {
      path: 'manager',
      component: () => import('@/views/version/manager'),
      name: 'version-manager',
      meta: {
        title: 'versionManager'
      }
    }]
}
export default versionRouter
