/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const transpondRouter = {
  path: '/transpond',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  name: 'transpond',
  meta: {
    title: 'transpond',
    icon: 'guide',
    permissions: ['Admin.Transpond']
  },
  children: [{
    path: '',
    component: () => import('@/views/transpond/index'),
    name: 'transpond-config',
    meta: {
      title: 'transpondConfig'
    }
  },
    {
      path: 'redundancy',
      component: () => import('@/views/transpond/redundancy'),
      name: 'redundancy',
      meta: {
        title: 'redundancy'
      }
    },
    {
      path: 'dataCache',
      component: () => import('@/views/transpond/data-cache'),
      name: 'data-cache',
      meta: {
        title: 'dataCache'
      }
    },
    {
      path: 'alarmCache',
      component: () => import('@/views/transpond/alarm-cache'),
      name: 'alarm-cache',
      meta: {
        title: 'alarmCache'
      }
    }
  ]
}
export default transpondRouter
