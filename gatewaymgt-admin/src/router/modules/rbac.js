/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const rbacRouter = {
  path: '/rbac',
  component: Layout,
  redirect: 'noRedirect',
  alwaysShow: true, // will always show the root menu
  name: 'rbac',
  hidden: true,
  meta: {
    title: 'rbac',
    icon: 'user',
    permissions: ['Admin.RBAC']
  },
  children: [
    // {
    //   path: '/organization-units',
    //   component: () => import('@/views/organization-units'),
    //   name: 'ou-list',
    //   meta: {
    //     title: 'ou',
    //     // permissions: ['Admin.Organization.Manage']
    //   }
    // },
    {
      path: '/roles',
      component: () => import('@/views/roles'),
      name: 'roles-list',
      meta: {
        title: 'role'
        // permissions: ['Admin.RBAC.Role.Manage']
      }
    },
    {
      path: '/users',
      component: () => import('@/views/users'),
      name: 'users-list',
      meta: {
        title: 'user',
        // permissions: ['Admin.RBAC.User.Manage']
      }
    },
    {
      path: '/permissions',
      component: () => import('@/views/permissions'),
      name: 'permission-list',
      meta: {
        title: 'permissions'
        // permissions: ['Admin.RBAC.Permission.Manage']
      }
    }
  ]
}
export default rbacRouter
